// C++ implementation of search and insert 
// operations on Trie 
#include <iostream>
#include<string>
using namespace std;

const int ALPHABET_SIZE = 26;

//struct for words
struct Words
{
	string tu, nghia;
};
// trie node 
struct TrieNode
{
	struct TrieNode *children[ALPHABET_SIZE];

	// isEndOfWord is true if the node represents 
	// end of a word 
	bool isEndOfWord;
	string meaning;
};

// Returns new trie node (initialized to NULLs) 
struct TrieNode *getNode(void)
{
	struct TrieNode *pNode = new TrieNode;

	pNode->isEndOfWord = false;

	for (int i = 0; i < ALPHABET_SIZE; i++)
		pNode->children[i] = NULL;

	return pNode;
}

// If not present, inserts key into trie 
// If the key is prefix of trie node, just 
// marks leaf node 
void insert(struct TrieNode *root, string key)
{
	struct TrieNode *pCrawl = root;

	for (int i = 0; i < key.length(); i++)
	{
		int index = key[i] - 'a';
		if (!pCrawl->children[index])
			pCrawl->children[index] = getNode();

		pCrawl = pCrawl->children[index];
	}

	// mark last node as leaf 
	pCrawl->isEndOfWord = true;
}
void insert_meaning(struct TrieNode *root, string key, string mean) 
{
	struct TrieNode *pCrawl = root;

	for (int i = 0; i < key.length(); i++)
	{
		int index = key[i] - 'a';
		if (pCrawl->children[index]->isEndOfWord==true)
			pCrawl->children[index]->meaning = mean;

		pCrawl = pCrawl->children[index];
	}
}
void meaning(struct TrieNode *root, string key)
{
	struct TrieNode *pCrawl = root;
	string chot;
	for (int i = 0; i < key.length(); i++)
	{
		int index = key[i] - 'a';
		if (pCrawl->children[index]->isEndOfWord)
			chot = pCrawl->children[index]->meaning;
		pCrawl = pCrawl->children[index];
	}
	cout << key << " : " << chot << endl;
}
// Returns true if key presents in trie, else 
// false 
bool search(struct TrieNode *root, string key)
{
	struct TrieNode *pCrawl = root;

	for (int i = 0; i < key.length(); i++)
	{
		int index = key[i] - 'a';
		if (!pCrawl->children[index])
			return false;

		pCrawl = pCrawl->children[index];
	}

	return (pCrawl != NULL && pCrawl->isEndOfWord);
}
// function to check if current node is leaf node or not 
bool isLeafNode(struct TrieNode *root)
{
	return root->isEndOfWord != false;
}

bool isEmpty(struct TrieNode *root)
{
	for (int i = 0; i < ALPHABET_SIZE; i++)
		if (root->children[i])
			return false;
	return true;
}
//function to delete word
struct TrieNode *remove(struct TrieNode *root, string key, int depth)
{
	if (!root)	return NULL;

	if (depth == key.size())
	{
		if (root->isEndOfWord)		
			root->isEndOfWord = false;
		if (isEmpty(root))
		{
			free(root);
			root = NULL;
		}

		return root;
	}

	int index = key[depth] - 'a';
	root->children[index] = remove(root->children[index], key, depth + 1);

	if (isEmpty(root) && !root->isEndOfWord)
	{
		free(root);
		root = NULL;
	}

	return root;
}
// function to display the content of Trie 
void display(struct TrieNode* root, char str[], int level)
{
	// If node is leaf node, it indiicates end 
	// of string, so a null charcter is added 
	// and string is displayed 
	if (isLeafNode(root))
	{
		str[level] = '\0';
		cout << str << endl;
	}

	int i;
	for (i = 0; i < ALPHABET_SIZE; i++)
	{
		// if NON NULL child is found 
		// add parent key to str and 
		// call the display function recursively 
		// for child node 
		if (root->children[i])
		{
			str[level] = i + 'a';
			display(root->children[i], str, level + 1);
		}
	}
}

// Driver 
int main()
{
	// Input keys (use only 'a' through 'z' 
	// and lower case) 
	Words word[11];
	//
	word[0].tu = "flower"; word[0].nghia = "bong hoa";
	word[1].tu = "flow"; word[1].nghia = "chay";
	word[2].tu = "friend"; word[2].nghia = "ban be";
	word[3].tu = "argumentative"; word[3].nghia = "co ly, co logic";
	word[4].tu = "arrangement"; word[4].nghia = "su sap xep, su sap dat";
	word[5].tu = "array"; word[5].nghia = "danh sach";
	word[6].tu = "a"; word[6].nghia = "mot";
	word[7].tu = "bear"; word[7].nghia = "con gau";
	word[8].tu = "blue"; word[8].nghia = "mau xanh duong";
	word[9].tu = "bean"; word[9].nghia = "dau, hot, hat";

	int n = 10;

	struct TrieNode *root = getNode();
	
	// Construct trie 
	for (int i = 0; i < n; i++)
		insert(root, word[i].tu);
	//nhap
	int level = 0;
	char str[20];

	// Displaying content of Trie 
	cout << "mini demo" << endl << endl;
	cout << "======my list====== " << endl;
	display(root, str, level);
	cout << "..................." << endl;
	//insert the meaning
	for (int i = 0; i < n; i++)
		insert_meaning(root, word[i].tu, word[i].nghia);
	//for (int i = 0; i < n; i++)
	//	xuatnghia(root, word[i].tu);
	
	string w;
	cout << "search for:   ";
	do {
		cin >> w;
		if(!search(root, w))
			cout << "not exist! please insert the word on the list above:" << endl;
	}
	while (!search(root,w));
	meaning(root, w);
	//insert a word
	cout << "# insert a word to dictionary # " << endl;
	cout << "word: "; cin >> word[10].tu; 
	cout << "meaning: "; cin >> word[10].nghia;
	insert(root, word[10].tu);
	insert_meaning(root, word[10].tu, word[10].nghia);
	cout << "......................" << endl;
	cout << "dictionary after adding " << endl;
	display(root, str, level);
	//remove word
	cout << "# remove word # " << endl;
	string rm;
	do {
		cin >> rm;
		if (!search(root, rm))
			cout << "please insert the word on the list above:" << endl;
	} while (!search(root, rm));
	root = remove(root, rm, 0);
	cout << "......................" << endl;
	cout << "dictionary after removing " << endl;
	display(root, str, level);
	
	while (true)
	{

	};
	return 0;
}
