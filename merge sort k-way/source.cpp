#include <iostream>
#include <vector>
#include <queue> 
#include <stdio.h>
#include<limits.h> 
#include<time.h>
#include <cstdlib>
using namespace std;

#define length 10000000 // 40000000 50000000 30000000 60000000 70000000 80000000 90000000 100000000
#define K_way 2 // 2 10 20 40 50 100 500 1000 10000 50000 
void ReadFileBin(int *&a)
{
	long count = 1;
	FILE *FileIn = new FILE;
	FileIn = fopen("test.bin", "rb");
	if (!FileIn)
	{
		return;
	}

	a = new int[length];
	a[length] = NULL;
	for (long i = 0; i < length; i++)
	{
		fscanf(FileIn, "%lf", &a[i]);
		//cout << i <<" ";
	}
	fclose(FileIn);
	//free(a);
}

void mergeSort_K_Way(int *&a)
{
	long element = length / K_way;

	priority_queue<int, vector<int>, std::greater<int> >pq; // tang dan 

	////////priority_queue<double>pq;//giam dan

	for (long r = 0; r < K_way; r++) {
		for (long c = 0; c < element; c++)
		{
			pq.push(a[r*element + c]); // a[r][c]
		}
	}

	while (pq.size()) {
		//cout << pq.top() << " ";
		pq.top();
		pq.pop();
	}
}


int main()
{
	
	int *a;
	ReadFileBin(a);
	clock_t t;
	t = clock();

	mergeSort_K_Way(a);
	//caculating run time
	t = clock() - t;
	printf("%f \n", ((float)t) / CLOCKS_PER_SEC);
	
	while (true)
	{
	}
	return 0;
}
